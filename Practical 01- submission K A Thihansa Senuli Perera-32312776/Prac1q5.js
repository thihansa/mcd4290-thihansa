//Practical 01
//Question 5

let year;
let yearNot2015Or2016;
year = 2000;
yearNot2015Or2016 = year === 2015 || year === 2016;
console.log(!yearNot2015Or2016);

//inputting year 2015 as an example
year = 2015;
yearNot2015Or2016 = year === 2015 || year === 2016;
console.log(!yearNot2015Or2016);
